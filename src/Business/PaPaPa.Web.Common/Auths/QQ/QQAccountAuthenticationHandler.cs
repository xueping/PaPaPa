﻿using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Web.Common.Auths.QQ
{
    public class QQAccountAuthenticationHandler : AuthenticationHandler<QQAccountAuthenticationOptions>
    {
        private System.Net.Http.HttpClient httpClient;

        public QQAccountAuthenticationHandler(System.Net.Http.HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
        protected override Task<Microsoft.Owin.Security.AuthenticationTicket> AuthenticateCoreAsync()
        {
            throw new NotImplementedException();
        }
    }
}
