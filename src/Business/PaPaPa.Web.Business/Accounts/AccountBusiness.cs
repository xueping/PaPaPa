﻿using Framework.Common.EnumOperation;
using Framework.Common.ExceptionOperation;
using Framework.Mapping.Base;
using PaPaPa.Core.Users;
using PaPaPa.Models.BasicData;
using PaPaPa.Models.Accounts;
using PaPaPa.Web.Models.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PaPaPa.Web.Business.Accounts
{
    public class AccountBusiness
    {
        public async static Task<UserModel> FindAsync(LoginViewModel model)
        {
            var user = await UserCore.FindUserByUserName(model.UserName);

            if (user.Password != model.Password)
            {
                throw new GException(EnumHelper.GetEnumDescription(ErrorCode.PasswordError).Description);
            }

            var mapper = new MapperBase<UserModel, User>();
            return mapper.GetModel(user);
        }

        public async static Task<UserModel> CreateAsync(RegisterViewModel model)
        {
            var registerMapper = new MapperBase<RegisterViewModel, User>();
            var user = registerMapper.GetEntity(model);

            await UserCore.CreateUser(user);

            var userMapper = new MapperBase<UserModel, User>();
            return userMapper.GetModel(user);
        }

        public async static Task ModifyPasswordAsync(int id, string oldPassword, string newPassword)
        {
            await UserCore.ModifyPasswordAsync(id, oldPassword, newPassword);
        }

        public async static Task<UserBasicDataViewModel> GetUserBasicDataByIdAsync(int id)
        {
            var user = await UserCore.FindUserById(id);
            var mapper = new MapperBase<UserBasicDataViewModel, User>();

            return mapper.GetModel(user);
        }

        public async static Task ModifyUserBasicData(UserBasicDataViewModel model)
        {
            var user = await UserCore.FindUserById(model.Id);
            var mapper = new MapperBase<UserBasicDataViewModel, User>();
            var entity = mapper.GetEntity(model, user);
            await UserCore.ModifyUserBasicData(entity);
        }
    }
}
